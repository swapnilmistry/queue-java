public class Queue<T> implements QueueInterface<T>{
    private T[] queueItems;
    private int maxQueueSize;
    private int currentQueueSize;

    public Queue(int maxQueueSize) {
        this.queueItems = (T[]) new Object[maxQueueSize];
        this.maxQueueSize = maxQueueSize;
        this.currentQueueSize = 0;
    }
    @Override
    public boolean isEmpty() {
        return this.currentQueueSize == 0;
    }

    @Override
    public void enqueue(T item) throws CustomQueueException {
        if (currentQueueSize == this.maxQueueSize) {
            throw new CustomQueueException("Cannot enqueue item. Max size reached.");
        }
        this.currentQueueSize += 1;
        this.queueItems[this.currentQueueSize -1] = item;
    }

    @Override
    public T dequeue() throws CustomQueueException {
        if (this.currentQueueSize == 0) {
            throw new CustomQueueException("Cannot dequeue item. Queue is already empty");
        }
        T removedItem = this.queueItems[0];
        // Shift all elements after dequeue
        for (int i = 0; i < this.currentQueueSize - 1; i++) {
            this.queueItems[i] = this.queueItems[i+1];
        }
        this.currentQueueSize -= 1;
        return removedItem;
    }

    @Override
    public void dequeueAll() {
        this.currentQueueSize = 0;
    }

    @Override
    public T peek() throws CustomQueueException {
        if (this.currentQueueSize == 0) {
            throw new CustomQueueException("Cannot peek item. Queue is empty");
        }
        return this.queueItems[0];
    }

    @Override
    public int size() {
        return this.currentQueueSize;
    }

    @Override
    public String toString() {
        String stringifiedQueueItems = "";
        for (int i = 0; i < this.currentQueueSize; i++) {
            stringifiedQueueItems += this.queueItems[i];
            if (i < this.currentQueueSize - 1) {
                stringifiedQueueItems += ", ";
            }
        }
        return stringifiedQueueItems;
    }
}
