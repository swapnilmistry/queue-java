public class Main {
    public static void main(String[] args) {
        try {
            String[] names = {"Swapnil", "Bhavin", "Russel", "Linston", "Nixon", "Pooja", "John", "Joe", "Albert", "Surbhi"};
            Queue<String> myQueue = new Queue<>(10);
            for (int i = 0; i < names.length; i++) {
                myQueue.enqueue(names[i]);
                String currentQueueStatus = myQueue.toString();
                System.out.println("Queue after adding " +names[i]);
                System.out.println(currentQueueStatus + "\n");
            }

             // Exception thrown since we will exceed queue capacity
             // myQueue.enqueue("Robert");

            for (int j = 0; j < 3; j++) {
                String removedItem = myQueue.dequeue();
                String currentQueueStatus = myQueue.toString();
                System.out.println("Queue after removing: " +removedItem);
                System.out.println(currentQueueStatus + "\n");
            }

            String itemAtStartOfQueue = myQueue.peek();
            System.out.println("Item at start of queue is: " +itemAtStartOfQueue);

            int queueSize = myQueue.size();
            System.out.println("Items in queue: " +queueSize);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}