public interface QueueInterface<T> {
    public boolean isEmpty();
    public void enqueue(T item) throws CustomQueueException;
    public T dequeue() throws CustomQueueException;
    public void dequeueAll();
    public T peek() throws CustomQueueException;
    public int size();
}
